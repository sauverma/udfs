package com.zeotap.udfs;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.hive.ql.exec.UDF;

/**
 * 
 * @author sauverma
 *
 */

public class IPExtractor extends UDF {
	private static final String IPADDRESS_PATTERN = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
			+ "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
			+ "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

	private static final Pattern pattern = Pattern.compile(IPADDRESS_PATTERN);

	public String evaluate(String str) {
		Matcher matcher = pattern.matcher(str);
		if (matcher.matches())
			return str;
		else
			return "1.2.3.4";
	}
}
